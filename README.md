# DevOps Academy Lab 1: CI/CD pipeline

This project guides you through setting up a CI/CD pipeline in GitLab that deploys a container based application running
in Azure Kubernetes Service (AKS).

## Preparations

You should have made the following preparations:

- You need to install
  - [Node JS](https://nodejs.org)
  - [Kubectl](https://kubernetes.io/docs/tasks/tools/)
  - [Azure CLI](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli)
  - It's recommended that you install [K9S](https://k9scli.io/) to explore the deployed program
- You should have access to GitLab at [https://innersource.soprasteria.com]
- You should have access to to the lab AKS cluster. To verify:
  - `az login --tenant 08e7ef95-591f-453e-929c-7a4a6688dc28`
  - `az aks list --query "[].name"`
  - `az aks get-credentials --resource-group devops --name devops-cluster`
  - `kubectl create namespace your-personal-namespace`

## High level steps

The steps of the lab are as follows:

1. You need to create your own namespace in AKS to separate your stuff from others
2. You need to create a GitLab repository which can deploy to AKS
   - You have to create Kubernetes credentials that the GitLab CI/CD pipeline will use to execute Kubectl
   - You need the GitLab CI/CD pipeline to be able to upload a Docker image to a Docker Registry (as this is done
     internally in GitLab, this doesn't require work)
   - You need AKS to be able to download the Docker image from the Docker Registry, to do so AKS needs an access token
     to the docker registry.
3. You need to update this code to reflect your namespace, and you need to decide on a URL for your application
4. When you push the initial code it should be available at your URL. When you make changes, these changes will
   automatically be reflected in your running application

## Detailed steps

### Prepare the application environment (Kubernetes)

Set up your Kubernetes namespace:

1. Log in to your Azure account with the Azure CLI: `az login --tenant 08e7ef95-591f-453e-929c-7a4a6688dc28`
   > **Note:** Use your soprasteria.com account.
2. Connect Kubectl to the AKS Kubernetes
   cluster: `az aks get-credentials --resource-group devops --name devops-cluster`
   > **Note:** You now have Kubernetes credentials in your .kube/config file to access the Kubernetes API.
3. Check that you can access the Kubernetes cluster: `kubectl get namespaces`
   > **Note:** You should get something like:
   >
   > ```
   > NAME                 STATUS   AGE
   > default              Active   7d5h
   > ...
   > ```
4. Create your own namespace: `kubectl create namespace <your namespace>`
   > **Note:** Namespaces are separate "areas" in a Kubernetes cluster. You can deploy things to your namespace without
   > affecting other users. Your namespace should contain your name so that you don't accidentally use the same
   > namespace as someone else. Example `kubectl create namespace team-johannes-hanso`
5. You need to create a [service account](https://kubernetes.io/docs/concepts/security/service-accounts/) which Gitlab
   can use to talk to the AKS cluster:

   - download the service account config file [gitlab-service-account.yaml](./setup/gitlab-service-account.yaml),
     and place it on your local file system .
   - Run `kubectl --namespace <your namespace> apply -f <local path>/gitlab-service-account.yaml`
     - **Example:
       ** `kubectl --namespace team-johannes-hanso apply -f C:\Users\hmartinsen\Downloads\gitlab-service-account.yaml`

   > **Note:** `kubectl apply` updates the configuration of the cluster by defining _resources_ that should exist in a
   > cluster. Example resources are `deployments` (running software), service accounts and services (load balanced
   > network accessible deployments)

6. Create a "token" for GitLab to use the service account, and limit how long it is
   valid: `kubectl --namespace <your namespace> create token gitlab-service-account --duration=1440h`

   - **Example:** `kubectl --namespace team-johannes-hanso create token gitlab-service-account --duration=1440h`

The token from the last step is input to set up your Gitlab project.

### Set up the Version Control and CI/CD Pipeline

Set up your Gitlab project:

1. Create a GitLab project for your code.
   - Go to the [lab group for the spring of 2023](https://innersource.soprasteria.com/devops-akademiet/lab-2024-spring) on Innersource (Sopra
     Steria's GitLab instance)
   - In ths group, click **New project**
   - Select **Create from Template**
   - Select **Group** (It is a tab in the middle of the page)
   - Find **Devops Lab Template** and click **Use template**
   - Enter a unique project name (**Example:** `team-johannes-hanso`) and click **Create project**
2. Now, you need to copy the token you created earlier in the Kubernetes Namespace preparation, into GitLabs CI/CD
   variables:

   - In your GitLab project > **Settings** > **CI/CD** > **Variables**.
   - **Add variable** with the key `KUBECTL_TOKEN` with the result of the `create token` command you used earlier

   > **Note:** Please be conscious of whitespace and line breaks that might sneak into the token when copy and pasting.
   >
   > **For advanced users:** At this time you might want to clone the repository down locally to use your favorite IDE.
   > Alternatively you can use the built-in editor in GitLab to do the edits and commits.

3. Add a [deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html#gitlab-deploy-token) under
   **Settings** > **Repository** > **Deploy tokens** with the Scope: `read_registry` (NB: **NOT** `read_repository`).
   It is vital that you give the deploy token the name `gitlab-deploy-token`

   > You don't need to save the values from the deploy token, as GitLab automatically makes these available
   > in the pipeline as variables $CI_DEPLOY_USER and $CI_DEPLOY_TOKEN 

4. Update `.gitlab-ci.yaml` with your `KUBECTL_NAMESPACE` in your repository

   - This can be done by selecting the file `.gitlab-ci.yaml` under **Code** > **Repository** in the project and
     pushing **Edit**.
   - Do the changes to `KUBECTL_NAMESPACE`
     - **Example:** `KUBECTL_NAMESPACE: team-johannes-hanso`
   - Now select **Commit changes**.

5. Commit (and push if you are working locally) your code. Your project should now build

   > **Note:** You can follow your build by selecting **CI/CD** > **Pipelines**. When the `deploy` stage is finished OK,
   > you can proceed.

6. Find the production environment under **Operate** > **Environments** and click **Open** to see the code running

7. Make a change to the file `src/app.tsx`, commit (and push), after the CI/CD pipeline have finished running you will
   see the changes when you refresh the application.

**Done!** You have now implemented an application using Continuous Deployment.

---

# DevOps Academy Lab 2: Streamlining change approval

Also called **Lightweight change approval process** in Accelerate.

> Research by DevOps Research and Assessment (DORA), finds that change approvals are best implemented through peer
> review during the development process, supplemented by automation to detect, prevent, and correct bad changes
> early in the software delivery life cycle.

Source: [DORA.dev](https://dora.dev/devops-capabilities/process/streamlining-change-approval/)

A good way to achieve this is to enforce that changes that are deployed are approved by another team member as
a Merge Request (also known as a Pull request). This can be implemented on Gitlab:

1. Update `.gitlab-ci.yaml` to only execute the `deploy` step on the main branch
   > **Example:**
   >
   > ```
   > ...
   > deploy:
   >  only:
   >    - main
   >  image: dtzar/helm-kubectl:3.10
   > ...
   > ```
2. In Gitlab **Protect** the `main` branch under **Settings** > **Repository** > **Protected branches**
   > **Hint:** The `main` branch is already protected by default, and it should only be necessary to set **Allowed to
   > push and merge** to **No one**.
3. You can specify rules for Merge Requests under **Settings** > **Merge requests**
   > **Hint:** Look through the **Approval settings** specifically to prevent any unwanted bypassing of the process

# DevOps Academy Lab 3: Automated Testing

In this lab you will add another step to perform a simple smoke test for the application.

1. Add another `stage`in the top of the `.gitlab-ci.yml` file. Call it `smoke-test`.
   > **Example:**
   >
   > ```
   > stages:
   > - build
   > - publish
   > - deploy
   > - smoke-test
   > ...
   > ```
2. Add another `job` to the bottom of the `.gitlab-ci.yml` file. Create a job that installs `curl` to fetch our web
   application and look for a known pattern.
   > **Example:**
   >
   > ```
   > ...
   > smoke-test:
   >  image: ubuntu:latest
   >  stage: smoke-test
   >  needs: [ deploy ]
   >  script:
   >    - apt update && apt upgrade -y
   >    - apt install curl -y
   >    - echo "Expecting <title>Hello World</title> in server response."
   >    - curl http://$KUBECTL_NAMESPACE.$DOMAIN > response.tmp
   >    - echo "Server response:"
   >    - cat response.tmp
   >    - cat response.tmp | grep "<title>Welcome"
   > ```
3. Try and break the test. :-D

## DevOps Academy Lab 3b: Automated Testing with unit tests

If you have the time, and you have git and an IDE on your workstation, you might want to try this one as well.

The code base comes with a ready-made component named `<CounterButton />`. You can add this button to the `app.tsx`
code:

1. Add `import { CounterButton } from "./components/counterButton";` at the top of `app.tsx`
2. Add `<CounterButton />` in the body of the `function App`
3. Commit and push the change
4. Check the code on your website when the build has completed

Unfortunately, the component contains a bug. Fortunately `src/components/__tests__/counterButton.test.tsx` contains
a test that will let you catch this bug before it catches you. Unfortunately this is not included in the run.
This is because it requires a slightly complicated setup.

1. Add the necessary dependencies:
    - (This can all be installed in a single command:
      `npm install -D jest @types/jest ts-jest jest-environment-jsdom @testing-library/react @testing-library/user-event`)
   - The jest test runner: `npm install -D jest`
   - Typescript definitions for jest: `npm install -D @types/jest`
   - Typescript _compiler_ for jest: `npm install -D ts-jest`
   - Plugin to make jest run with a simulated DOM in Node.js: `npm install -D jest-environment-jsdom`
   - Testing library for React: `npm install -D @testing-library/react`
   - Testing library for simulating mouse clicks etc: `npm install -D @testing-library/user-event`
2. Create a file named `jest.config.js` with the following content:
   ```javascript
   /** @type {import('ts-jest').JestConfigWithTsJest} */
   module.exports = {
     preset: 'ts-jest',
     testEnvironment: "jsdom"
   };
   ```
3. In `package.json` modify the `test` script to run jest: `"test": "jest && tsc && prettier --check src/",`
   (alternatively, you can run `npm pkg set scripts.test="jest && tsc && prettier --check src/"`)

When you commit, your tests will run in Gitlab.

## DevOps Academy Lab 3c: Improving the smoke test

You will find that the smoke test always succeeds. If you had a failing unit test, the project will try to deploy
anyway (this is due to a problem in `package.json`). The smoke test will anyway pass! The reason for this is that
Kubernetes will leave the old pod running. To have a more robust test, we can instead check the commit.txt file
that is generated on the server to see if it is the one we expected.

Hint: Use the file `check-deployed-commit.txt`

---

# Continuing further

## Testing the application locally

1. `npm install`
2. `npm run dev`
3. Check out the application at `http://localhost:5173`. Changes in `src/app.tsx` will show up automatically
4. You can also build and test the docker file locally (if you have docker installed):
   - `docker build . --tag devops-lab`
   - `docker run -p 8080:80 devops-lab`

### Implement review apps

You can make Gitlab deploy multiple copies of the same application in the same namespace based on branch rules.
See the [Devops Review Sample App](https://innersource.soprasteria.com/devops-akademiet/devops-review-apps) for an
example.

You can implement that commits to branches matching specific patterns deploy a copy of the application to
a separate URL that are automatically removed after a specified time

Make the following changes to `.gitlab-ci.yaml`:

1. Create a `deploy-review` starting with the `deploy` task as a template, but only trigger on the branches you want
2. Add a parameter to the `helm` command to adjust the names in the `ingress`, `service` and `deployment`, based on
   the automatic variable `$CI_COMMIT_REF_SLUG`. I added `--set suffix=-$CI_COMMIT_REF_SLUG`
3. Use the variable in the `kubernetes/values.yaml` and `kubernetes/template/*.yaml` to create copies of everything
4. Add a `stop-review` job which does `kubectl delete` where `deploy-review` does `kubectl apply`. The `stop`-job
   could be marked with `when: manual` to avoid running automatically and `deploy-review` should reference it
   as `environment` > `on_stop`. Also add `environment` > `auto_stop_in` to `deploy-review`

When working with helm templates, it's useful to remember that you can verify the output locally by executing
`helm template` manually instead of having to push every change to verify that it's working.

### Implement static security checks

GitLab has build-in support for static security analysis with SAST. You can enable this under
**Secure** > **Security Configuration**. You can try to enable SAST and look at the report under !!TODO!!


## Create a Kubernetes cluster

[See documentation for creating your own cluster](./README-cluster.md)
