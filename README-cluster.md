# Setting up a cluster

This document describes two approaches:

- Running on Azure with AKS and Nginx Ingress
- Creating a cluster running on your local machine

## Prerequisites

You must install the following:

- [Docker](https://docs.docker.com/get-docker/) to follow this guide
- [Kubectl](https://kubernetes.io/docs/tasks/tools/)
- [Azure CLI](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli) (for the Azure variant)
- [Kind](https://kind.sigs.k8s.io/) (for the local variant)

## First: Publishing a Docker image

1. Create a project in GitLab on [https://innersource.soprasteria.com] (see [README](./README.md))
2. Under your user profile, create [a personal access token](https://innersource.soprasteria.com/-/user_settings/personal_access_tokens)
   - Token name can be anything you want. I'm using "gitlab"
   - Scopes should include `read_registry` and `write_registry`
3. Log in to docker
   - `docker login registry-innersource.soprasteria.com` - use any username and the token from the previous step
4. Build and publish your docker image
   - `npm run build`
   - `docker build . -t devops-lab`
   - `docker tag devops-lab registry-innersource.soprasteria.com/<your-project-name>:latest`
   - `docker push registry-innersource.soprasteria.com/<your-project-name>:latest`

### Deploy the app into a cluster

Follow one of the instructions below to set up a cluster. When you have created a cluster, follow these steps to deploy manually:

1. Create a namespace for your deployment
2. Create a docker secret with the GitLab token (from above) in the namespace:
   > `kubectl create secret docker-registry pull-secret --docker-username=<token name> --docker-password=<token secret> --docker-server=registry-innersource.soprasteria.com`
3. Deploy the application with helm and kubectl:
   > `helm template kubernetes --set image=registry-innersource.soprasteria.com/<your-project-name> --set domain=example.com | kubectl apply -f -`
4. As an alternative to using your own app, you can deploy Microsoft's [sample application](https://github.com/dotnet/dotnet-docker/tree/main/samples/aspnetapp):
   `kubectl apply -f https://raw.githubusercontent.com/Azure/application-gateway-kubernetes-ingress/master/docs/examples/aspnetapp.yaml`
5. Verify the ingresses with `kubectl get ingress`

To access the application, update your hosts file:

`<external-ip> replaceme.example.com`

See the individual version to see how to determine your IP address.

### Creating an Azure Kubernetes cluster with Nginx Ingress

1. Log into Azure: `az login --tenant 85173d93-99ef-4dff-9b45-495719659133`
2. Create a Resource Group for the workshop `az group create --name devops --location eastus`
3. Create a Kubernetes cluster: `az aks create --resource-group devops --name devops-cluster --node-count 1 --tier free`
   > Note: Both the group name and cluster name are up to you. You don't need to call them `devops` and `devops-cluster`
4. Set up kubectl to access your cluster `az aks get-credentials --resource-group devops --name devops-cluster`
5. Check that you have access to the cluster `kubectl get namespace`
6. Install the Nginx ingress:
   - `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/cloud/deploy.yaml`
     > Note: This is a different version of the yaml from [kind](#creating-a-kind-cluster)
   - Wait for the pods to
     start `kubectl wait --namespace ingress-nginx --for=condition=ready pod --selector=app.kubernetes.io/component=controller --timeout=90s`
7. You can view Ingress pods and services in the namespace: `kubectl get all --namespace ingress-nginx`

Nginx will create a public IP address. Find this by
`kubectl get services --namespace ingress-nginx ingress-nginx-controller -o jsonpath="{.status.loadBalancer.ingress[].ip}"`
or `az network public-ip list --query "[].{name:name, ipAddress:ipAddress, tags:tags}"`

You have to ensure that your hostname points to this IP address, either by editing your hosts file or DNS.

Run `kubectl cluster-info` to get the Kubernetes control plane URL to set as `KUBECTL_SERVER` in `.gitlab-ci.yml` or as
a CI/CD Variable on the Project or Group. 

You can now [deploy your application](#deploy-the-app-into-a-cluster). You can try first with `setup/example-app` if
you struggle with the registry or secret.

1. [Build and publish your application to registry-innersource.soprasteria.com](#first-publishing-a-docker-image)
2. Create a docker secret with the GitLab token (from above) in the namespace:
   > `kubectl create secret docker-registry pull-secret --docker-username=<token name> --docker-password=<token secret> --docker-server=registry-innersource.soprasteria.com`
3. Deploy the app with helm:
   `helm template kubernetes --set image=registry-innersource.soprasteria.com/<your-project-name> --set domain=example.com | kubectl apply -f -`
4. Add a line with the hostname from the ingress to your hosts-file, e.g. `<ip address> replaceme.example.com`
5. You can now access the application host name in your browser

When you're done running on this cluster, you can delete it with `az group delete --name devops`

### Creating a Kind cluster

[Kind](https://kind.sigs.k8s.io/) lets you create and manage a kubernetes cluster running inside Docker. 

1. Create a cluster on your local machine: `kind create cluster --config setup/create-cluster.yaml`
   - `create-cluster.yaml` contains configuration to forward port 80 and 443 on your local machine to this cluster
   - If you don't need incoming http/https traffic, you can do `kind create cluster`
   - The `kind create cluster` command updates `~/.kube/config` to make `kubectl` connect to the new cluster
   - You can see the cluster nodes running with `docker ps`
   - You can see the nodes from the perspective of the cluster with `kubectl get nodes`
2. Install an Ingress controller (in our case: Nginx):
   - `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml`
     > Note: This is a specialized manifest for running in Kind
   - Wait for the pods to
     start `kubectl wait --namespace ingress-nginx --for=condition=ready pod --selector=app.kubernetes.io/component=controller --timeout=90s`

Your IP address will be `127.0.0.1` (loopback). To [deploy your application](#deploy-the-app-into-a-cluster), follow the steps from above. Briefly:

1. [Build and publish your application to registry-innersource.soprasteria.com](#first-publishing-a-docker-image)
2. Create a docker secret with the GitLab token (from above) in the namespace:
   > `kubectl create secret docker-registry pull-secret --docker-username=<token name> --docker-password=<token secret> --docker-server=registry-innersource.soprasteria.com`
3. Deploy the app with helm:
   `helm template kubernetes --set image=registry-innersource.soprasteria.com/<your-project-name> --set domain=example.com | kubectl apply -f -`
4. Add a line with the hostname from the ingress to your hosts-file, e.g. `127.0.0.1 replaceme.example.com`
5. You can now access the application host name in your browser

When you're done running on this cluster, you can delete it with `kind delete cluster`

### Creating an AKS cluster with Application Gateway (NO LONGER IN USE)

Please note: Application Gateway is pretty expensive. Remember to delete it after use.

1. Log into Azure: `az login --tenant 85173d93-99ef-4dff-9b45-495719659133`
2. Create a Resource Group for the workshop `az group create --name devops --location westeurope`
3. Create a Kubernetes cluster: `az aks create --resource-group devops --name <cluster name>`
4. Set up kubectl to access your cluster `az aks get-credentials --resource-group devops --name <cluster name>`
5. Check that you have access to the cluster `kubectl get namespace`
6. Create a Kubernetes cluster with an appGateway
   ingress: `az aks create --resource-group devops --network-plugin azure --enable-managed-identity --enable-addons ingress-appgw --appgw-name myApplicationGateway --appgw-subnet-cidr "10.225.0.0/16" --generate-ssh-keys --name <cluster name>`
   > See [Microsoft's documentation on AKS and AppGateway](https://learn.microsoft.com/en-us/azure/application-gateway/tutorial-ingress-controller-add-on-new)
   > for details

Nginx will create a public IP address. Find this by `az network public-ip list --query "[].{name:name, ipAddress:ipAddress, tags:tags}"`

You can now [deploy your application](#deploy-the-app-into-a-cluster). You can try first with `setup/example-app` if
you struggle with the registry or secret.

1. [Build and publish your application to registry-innersource.soprasteria.com](#first-publishing-a-docker-image)
2. Create a docker secret with the GitLab token (from above) in the namespace:
   > `kubectl create secret docker-registry pull-secret --docker-username=<token name> --docker-password=<token secret> --docker-server=registry-innersource.soprasteria.com`
3. Deploy the app with helm:
   `helm template kubernetes --set image=registry-innersource.soprasteria.com/<your-project-name> --set ingressClassName=azure-application-gateway | kubectl apply -f -`
4. Add a line with the hostname from the ingress to your hosts-file, e.g. `<ip address> replaceme.example.com`
5. You can now access the application host name in your browser
