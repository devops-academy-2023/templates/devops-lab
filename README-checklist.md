# Instructor checklist

This document describes how to verify that the cluster is ready for a workshop. This should be done less
than 12 hours before a workshop.

The document is a work in progress:

What can go wrong

- [ ] Wrong URL to service account setup
- [ ] Wrong URL to lab group
- [ ] Variables in .gitlab-ci.yaml
    - [ ] `KUBECTL_SERVER` variable in GitLab Group (from `kubectl cluster-info`)
    - [ ] Domain
- [ ] DNS setup for test domain (from `kubectl get services --namespace ingress-nginx ingress-nginx-controller -o jsonpath="{.status.loadBalancer.ingress[].ip}"`)

Overall, what's needed is to clone the repository, get a Kubectl token according to the instructions,
update `.gitlab-ci.yaml` according to instructions, update GitLab environment variables according to
instructions, see the build succeed and verify the DNS by seeing the code run on the URL from GitLab
environment.
