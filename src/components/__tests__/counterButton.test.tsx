import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { CounterButton } from "../counterButton";

describe("counter button", () => {
  it("updates counter on click", async () => {
    render(<CounterButton />);
    const button = await screen.findByText(/Clicked \d+ times/);
    await userEvent.click(button);
    await userEvent.click(button);
    expect(button.textContent).toBe("Clicked 2 times");
  });
});
