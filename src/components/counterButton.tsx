import React, { useState } from "react";

export function CounterButton() {
  const [clickCount, setClickCount] = useState(0);
  function handleClick() {
    setClickCount(clickCount - 1);
  }

  return <button onClick={handleClick}>Clicked {clickCount} times</button>;
}
