import React from "react";

export function App() {
  return (
    <>
      <h1>This is a dummy text - please update it in app.tsx</h1>
      <p>This is a paragraph</p>
    </>
  );
}
