#!/bin/bash

[ -z "$KUBECTL_NAMESPACE" ] && echo "missing variable KUBECTL_NAMESPACE" && exit 2
[ -z "$DOMAIN" ] && echo "missing variable DOMAIN" && exit 2
[ -z "$CI_COMMIT_SHORT_SHA" ] && echo "missing variable CI_COMMIT_SHORT_SHA" && exit 2

SECONDS=0
while (( SECONDS < ${TIMEOUT:-15} ))
do
  echo Checking http://$KUBECTL_NAMESPACE.$DOMAIN/commit.txt
  curl -s http://$KUBECTL_NAMESPACE.$DOMAIN/commit.txt | grep $CI_COMMIT_SHORT_SHA > /dev/null && exit 0
  sleep 3
done

echo curl http://$KUBECTL_NAMESPACE.$DOMAIN/commit.txt
curl http://$KUBECTL_NAMESPACE.$DOMAIN/commit.txt
exit 1

